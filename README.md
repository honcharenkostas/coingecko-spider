## coingecko website spider
### requirements:
python 3.8+

For manual launching:
```
python3 -m venv venv
source /venv/bin/activate
pip install -r requirements.txt
scrapy runspider coingecko_spider.py
```
For  launching from docker:
```
docker build -t coin-spider .
docker run -it -v /full-path-to/coingecko-spider/storage:/usr/src/app/storage coin-spider
```

Spider will scrape all coins from coingecko website and store info per coin to csv ```coins.csv``` (Global storage) and to ```coins_yyyy-mm-dd_hh:mm:ss.csv``` (New coins, if to compare with previous scraping)