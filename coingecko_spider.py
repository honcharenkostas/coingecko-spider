import os
import json
from datetime import datetime as dt
import scrapy
import csv


class CoingeckoSpider(scrapy.Spider):

    name = "coingecko"

    RAW_HEADERS_LIST_PAGE = '''
        accept: */*
        accept-language: en-US,en;q=0.9,uk;q=0.8,ru;q=0.7
        cache-control: no-cache
        origin: https://www.coingecko.com
        pragma: no-cache
        referer: https://www.coingecko.com/
        sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"
        sec-ch-ua-mobile: ?0
        sec-fetch-dest: empty
        sec-fetch-mode: cors
        sec-fetch-site: same-site
        user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36
        '''

    RAW_HEADERS_COIN_PAGE = '''
        accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
        accept-language: en-US,en;q=0.9,uk;q=0.8,ru;q=0.7
        cache-control: no-cache
        pragma: no-cache
        referer: https://www.coingecko.com/en/coins/all
        sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"
        sec-ch-ua-mobile: ?0
        sec-fetch-dest: document
        sec-fetch-mode: navigate
        sec-fetch-site: same-origin
        sec-fetch-user: ?1
        upgrade-insecure-requests: 1
        user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36
        '''

    FILE_NAMES = [
        'storage/coins.csv',
        f'storage/coins_{dt.now().strftime("%Y-%m-%d_%H:%M:%S")}.csv'
    ]

    FIELD_NAMES = [
        'Datetime',
        'Id',
        'Project Name',
        'Ticker',
        'Market Cap',
        'Website',
        'Explorers',
        'Wallets',
        'Community',
        'Source Code',
        'Exchangers',
    ]

    coin_ids = set()

    def __init__(self, *args, **kwargs):
        super(CoingeckoSpider, self).__init__(*args, **kwargs)

    def start_requests(self):
        # create main file if not exists
        main_file = self.FILE_NAMES[0]
        if not os.path.exists(main_file):
            open(main_file, 'w')

        # get coin_ids by previous scraping
        with open(main_file) as f:
            reader = csv.reader(f)
            for row in list(reader)[1:]:
                if len(row) > 2 and row[1]:
                    self.coin_ids.add(row[1])

        # scrape home page
        headers = self._convert_raw_headers(self.RAW_HEADERS_LIST_PAGE)
        url = f'https://api.coingecko.com/api/v3/search?locale=en'
        yield scrapy.Request(
            url=url,
            callback=self.parse_list,
            headers=headers,
            dont_filter=True,
        )

    def parse_list(self, resp):
        headers = self._convert_raw_headers(self.RAW_HEADERS_COIN_PAGE)
        try:
            coins = json.loads(resp.text)['coins']
        except:
            coins = []

        for coin in coins:
            c_id = coin.get('id')
            if not c_id or c_id in self.coin_ids:
                continue

            url = f'https://www.coingecko.com/en/coins/{c_id}'
            yield scrapy.Request(
                url=url,
                callback=self.parse_coin,
                headers=headers,
                dont_filter=True,
                meta={
                    'c': {
                        'id': c_id,
                        'name': coin.get('name'),
                        'symbol': coin.get('symbol'),
                        'market_cap_rank': coin.get('market_cap_rank')
                    }
                }
            )

    def parse_coin(self, resp):
        c = resp.request.meta['c']

        result = {
            'Datetime': dt.now().strftime("%d/%m/%Y %H:%M:%S"),
            'Id': c.get('id'),
            'Project Name': c.get('name'),
            'Ticker': c.get('symbol'),
            'Market Cap': c.get('market_cap_rank'),
            'Website': '',
            'Explorers': '',
            'Wallets': '',
            'Community': '',
            'Source Code': '',
            'Exchangers': '',
        }

        # extract tags
        for row in resp.css('.coin-link-row'):
            title = row.css('.coin-link-title::text').extract_first('').strip()
            tags = row.css('.coin-link-tag::attr(href)').extract()
            tags = ', '.join(tags) if tags else ''

            if title in ['Website', 'Explorers', 'Wallets', 'Community', 'Source Code'] and tags:
                result[title] = tags

        # extract exchangers
        exchangers = set()
        for row in resp.css('#spot .coingecko-table tr'):
            exchanger_name = row.css('td:nth-child(2) a::text').extract_first('').strip()
            if exchanger_name:
                exchangers.add(exchanger_name)
        result['Exchangers'] = ', '.join(exchangers)

        self.write_to_csv(result)

        yield result

    def write_to_csv(self, row):
        for file_name in self.FILE_NAMES:
            if not os.path.exists(file_name):
                open(file_name, 'w')

            with open(file_name, 'r+') as f:
                writer = csv.DictWriter(f, fieldnames=self.FIELD_NAMES)

                # add headers
                reader = csv.reader(f)
                if len(list(reader)) == 0:
                    writer.writeheader()

                # add row
                writer.writerow(row)

    @staticmethod
    def _convert_raw_headers(raw_headers):
        result = {}
        for row in raw_headers.split('\n'):
            row = row.strip()
            parts = row.split(':')
            if parts and len(parts) >= 2:
                key_len = len(parts[0])
                val = row[key_len:]
                val = val[1:] if val.find(':') == 0 else val
                val = val.strip()
                if parts[0]:
                    result[parts[0]] = val

        return result
